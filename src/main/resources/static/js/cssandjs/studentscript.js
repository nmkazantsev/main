function showTests(){
    var testsId=document.getElementById("tests");
    if(testsId.style.display=="block"){
        testsId.style.display="none";
    }
    else{
        testsId.style.display="block";
    }
}
function parser(str){
    str=str.substring(1,str.length-1).split("#");
    for(let i=0;i<str.length;i++){
        str[i]=str[i].split(",");
    }
    return str;
}
function get(url,id){
    let htmlTables = "";
    let x = new XMLHttpRequest();
    x.open("GET", url, true);
    x.send(null);
    var str = "";
    x.onload = function () {
        if (x.status == 200) {
            str = x.responseText;
            var tables = str.split("}");
            for (let i = 0; i < tables.length - 1; i++) {
                let oneTabble = parser(tables[i]);
                for (let q = 0; q < oneTabble.length; q++) {
                    if(q!=0){
                        htmlTables += "<tr>";
                    }
                    for (let j = 0; j < oneTabble[q].length; j++) {
                        if(q==0){
                            htmlTables+="<h2>"+oneTabble[0][0]+"</h2>"
                            htmlTables += "<table>";
                        }
                        else if (q == 1) {
                            htmlTables += "<th>" + oneTabble[q][j] + "</th>";
                        } else {
                            htmlTables += "<td>" + oneTabble[q][j] + "</td>";
                        }
                    }
                    if(q!=0){
                        htmlTables += "<tr>";
                        htmlTables += "</tr>";
                    }
                }
                htmlTables += "</table>";
                htmlTables += "<p></p>";
            }
            document.getElementById(id).innerHTML=htmlTables;
        } else {
            alert("ошибка");
            return("ошибка");
        }
    }
}
function getTests(){
    let token = localStorage.getItem("logToken")
    get("/studentTests?token=" + token,"tests");
}