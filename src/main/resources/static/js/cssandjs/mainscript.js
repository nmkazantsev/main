function openAddForm(){
    var addId=document.getElementById("addForm");
           if(addId.style.display=="block"){
               addId.style.display="none";//метод изменения невидимости блока
           }
           else{
               addId.style.display="block";
           }
        }
    function openGroup(){
        var id=document.getElementById("groups");
        if(id.style.display=="block"){
            id.style.display="none";
        }
        else{
            id.style.display="block";
        }
    }
        function openChooseRole(){
            document.getElementById("allRoles").style.display="block";
        }
        function switchRole(i){
            var el=document.getElementById("roleText")
            if(i==1){
                el.innerHTML="Учитель";//метод изменения текста блока
            }
            else if(i==2){
                el.innerHTML="Ученик";
            }
            else{
                el.innerHTML="Администратор";
            }
            document.getElementById("allRoles").style.display="none";
        }
        function addUser(){
            //role login pass
            let login=document.getElementById("login").value;
            let password=document.getElementById("pass").value;
            let role=document.getElementById("roleText").innerText;
            if(role=="Учитель"){
                role="teacher";
            }
            else if(role=="Ученик"){
                role="student";
            }
            else if(role=="Администратор"){
                role="weakAdmin";
            }
            let token=localStorage.getItem("logToken")
            let x = new XMLHttpRequest();
            x.open("GET", "/adminAddUser?login=" + login + "&pass=" + password+"&roleText="+role+"&token="+token, true);
            x.send(null);
            x.onload = function () {
            }
        }

        function createGroup(){
            let groupName=document.getElementById("groupName").value;
            let token=localStorage.getItem("logToken");
            let x = new XMLHttpRequest();
            x.open("GET", "/adminAddGroup?name=" + groupName + "&token=" + token, true);
            x.send(null);
            x.onload = function () {
            }

        }

        function studentAddInGroup(){
            let groupInAdd=document.getElementById("groupName2").value;
            let studentLogin=document.getElementById("studentLogin").value;
            let token=localStorage.getItem("logToken");
            let x = new XMLHttpRequest();
            x.open("GET", "/adminAddStudentGroupConnection?student=" + studentLogin + "&group=" + groupInAdd + "&token="+token, true);
            x.send(null);
            x.onload = function () {
            }

        }
        function teacherAddInGroup(){
            let groupInAdd=document.getElementById("groupName3").value;
            let teacherLogin=document.getElementById("teacherLogin").value;
            let token=localStorage.getItem("logToken");
            let x = new XMLHttpRequest();
            x.open("GET", "/adminAddGroupTeacherConnection?teacherName=" + teacherLogin+"&groupName="+groupInAdd+"&token="+token, true);
            x.send(null);
            x.onload = function () {
            }
        }