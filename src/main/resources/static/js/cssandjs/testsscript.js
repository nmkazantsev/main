var questionNumber=0;
var nowQuestion=0;
var answers="";
var questions;
function getQuestions(){
    let location=document.location.href.split("&testId=");
    let testId=location[1];
    let token = localStorage.getItem("logToken")
    let url="/getTest?token="+token+"&testId="+testId;
    let x = new XMLHttpRequest();
    x.open("GET",url,true);
    x.send(null);
    x.onload = function () {
        if (x.status == 200) {
            questions=x.responseText.split("#");
            questionNumber=questions.length;
            alert(questions.length);
            document.getElementById("question").innerText=questions[nowQuestion];
        }
    }
}
function addAnswer(){
    let answerId=document.getElementById("answer").value;
    answers=answers+answerId+"#";
    answerId="";
    nowQuestion=nowQuestion+1;
    document.getElementById("answer").value="";
    if(questionNumber!=nowQuestion){
        document.getElementById("question").innerText=questions[nowQuestion];
    }
    else{
        let x = new XMLHttpRequest();
        var q="";//questions
        var a=answers.substring(0, answers.length - 1);
        for(let i=0;i<questions.length;i++){
            q+=questions[i];
            if(i!=questions.length-1){
                q+="#";
            }
        }
        let token = localStorage.getItem("logToken")
        let location=document.location.href.split("&testId=");
        let testId=location[1];
        let url="/sendAnswers?token="+token+"&questions="+encodeURIComponent(q)+"&otvets="+encodeURIComponent(a)+"&testId="+testId;
        x.open("GET", url,true);
        x.send(null);
        x.onload = function () {
            if (x.status == 200) {
                alert("Вы прошли тест, оценка "+x.responseText);
            } else {
                alert("ошибка");
            }
        }
    }
}