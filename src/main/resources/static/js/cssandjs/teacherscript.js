function openGroup() {
    var groupsId = document.getElementById("groups");
    if (groupsId.style.display == "block") {
        groupsId.style.display = "none";
    } else {
        groupsId.style.display = "block";
    }
}

function showTests() {
    var testsId = document.getElementById("tests");
    if (testsId.style.display == "block") {
        testsId.style.display = "none";
    } else {
        testsId.style.display = "block";
    }
}

function showTickets() {
    var ticketsId = document.getElementById("tickets");
    if (ticketsId.style.display == "block") {
        ticketsId.style.display = "none";
    } else {
        ticketsId.style.display = "block";
    }
}

function addTest() {
    let group = document.getElementById("groupName").value;
    let startDate = document.getElementById("startDate").value.split("-");
    let startTime = document.getElementById("startTime").value.split(":");
    let endDate = document.getElementById("endDate").value.split("-");
    let endTime = document.getElementById("endTime").value.split(":");
    let start = new Date(startDate[2], startDate[1] - 1, startDate[0], startTime[0], startTime[1]).getTime();
    alert(start);
    let end = new Date(endDate[2], endDate[1] - 1, endDate[0], endTime[0], endTime[1]).getTime();
    let testName = document.getElementById("testName").value;
    let token = localStorage.getItem("logToken");
    let x = new XMLHttpRequest();
    x.open("GET", "/teacherAddTest?nameGroup=" + encodeURIComponent(group) + "&timeStart=" + start + "&timeStop=" + end + "&token=" + token + "&nameTest=" + testName, true);
    x.send(null);
    x.onload = function () {
        if (x.status == 200) {
            alert("успешно");
        } else {
            alert("ошибка");
        }
    }
}

function addModule() {
    let module = document.getElementById("moduleName").value;
    let token = localStorage.getItem("logToken");
    let x = new XMLHttpRequest();
    x.open("GET", "/teacherAddModule?moduleName=" + encodeURIComponent(module) + "&token=" + token, true);
    x.send(null);
    x.onload = function () {
        if (x.status == 200) {
            alert("успешно");
        } else {
            alert("ошибка");
        }
    }
}

function addToTicket() {
    let question = document.getElementById("questionToTicket").value;
    let token = localStorage.getItem("logToken");
    let ticket = document.getElementById("ticketAddQuestionTo").value;
    let x = new XMLHttpRequest();
    x.open("GET", "/teacherAddTicketQuestionConnection?question=" + encodeURIComponent(question) + "&token=" + token + "&ticket=" + encodeURIComponent(ticket), true);
    x.send(null);
    x.onload = function () {
        if (x.status == 200) {
            alert("успешно");
        } else {
            alert("ошибка");
        }
    }
}

function addTicket() {
    let ticket = document.getElementById("ticketName").value;
    let token = localStorage.getItem("logToken");
    let x = new XMLHttpRequest();
    x.open("GET", "/teacherAddTicket?nameTicket=" + encodeURIComponent(ticket) + "&token=" + token);
    x.send(null);
    x.onload = function () {
        if (x.status == 200) {
            alert("успешно");
        } else {
            alert("ошибка");
        }
    }
}


function addTicketToM() {
    let ticket = document.getElementById("ticketAddTicketName").value;
    let module = document.getElementById("moduleAddTicketName").value;
    let token = localStorage.getItem("logToken");
    let x = new XMLHttpRequest();
    x.open("GET", "/teacherAddTicketToModule?nameTicket=" + encodeURIComponent(ticket) + "&token=" + token + "&namemodule=" + module);
    x.send(null);
    x.onload = function () {
        if (x.status == 200) {
            alert("успешно");
        } else {
            alert("ошибка");
        }
    }
}

function generateTicket() {
    let ticket = document.getElementById("generateTicketName").value;
    let token = localStorage.getItem("logToken");
    let numberOfQuestions = document.getElementById("numberOfQuestions").value
    let numberOfModules = document.getElementById("numberOfModules").value;
    let x = new XMLHttpRequest();
    x.open("GET", "/teacherGenerateTicket?nameTicket=" +
        encodeURIComponent(ticket) + "&token=" + token +
        "&ticket=" + encodeURIComponent(ticket) +
        "&numberOfQuestions=" + encodeURIComponent(numberOfQuestions) +
        "&numberOfModules=" + encodeURIComponent(numberOfModules)
    );
    x.send(null);
    x.onload = function () {
        if (x.status == 200) {
            alert("успешно");
        } else {
            alert("ошибка");
        }
    }
}

function addQuestion() {
    let question = document.getElementById("question").value;
    let answer = document.getElementById("answer").value;
    let module = document.getElementById("module").value;
    let token = localStorage.getItem("logToken");
    let x = new XMLHttpRequest();
    x.open("GET", "/teacherAddQuestion?nameQuestion=" + encodeURIComponent(question) + "&nameAnswer=" + encodeURIComponent(answer) + "&nameModule=" + encodeURIComponent(module) + "&token=" + token);
    x.send(null);
    x.onload = function () {
        if (x.status == 200) {
            alert("успешно");
        } else {
            alert("ошибка");
        }
    }
}

function showMarks() {
    var testsId = document.getElementById("marks");
    if (testsId.style.display == "block") {
        testsId.style.display = "none";
    } else {
        testsId.style.display = "block";
    }
}

function getTables() {
    let token = localStorage.getItem("logToken")
    get("/teacherMarks?token=" + token, "marks")
}

function get(url, id) {
    let htmlTables = "";
    let x = new XMLHttpRequest();
    x.open("GET", url, true);
    x.send(null);
    var str = "";
    x.onload = function () {
        if (x.status == 200) {
            str = x.responseText;
            var tables = str.split("}");
            for (let i = 0; i < tables.length - 1; i++) {
                let oneTabble = parser(tables[i]);
                for (let q = 0; q < oneTabble.length; q++) {
                    if (q != 0) {
                        htmlTables += "<tr>";
                    }
                    for (let j = 0; j < oneTabble[q].length; j++) {
                        if (q == 0) {
                            htmlTables += "<h2>" + oneTabble[0][0] + "</h2>"
                            htmlTables += "<table>";
                        } else if (q == 1) {
                            htmlTables += "<th>" + oneTabble[q][j] + "</th>";
                        } else {
                            htmlTables += "<td>" + oneTabble[q][j] + "</td>";
                        }
                    }
                    if (q != 0) {
                        htmlTables += "<tr>";
                        htmlTables += "</tr>";
                    }
                }
                htmlTables += "</table>";
                htmlTables += "<p></p>";
            }
            document.getElementById(id).innerHTML = htmlTables;
        } else {
            alert("ошибка получения " + url);
            return ("ошибка");
        }
    }
}
function parser(str) {
    str = str.substring(1, str.length - 1).split("#");
    for (let i = 0; i < str.length; i++) {
        str[i] = str[i].split(",");
    }
    return str;
}