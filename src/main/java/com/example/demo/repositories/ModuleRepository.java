package com.example.demo.repositories;

import com.example.demo.models.Module;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ModuleRepository extends JpaRepository<Module, Long> {
    Module findModuleById(Long id);

    List<Module> findAllByTeacherId(Long id);

    Module findModuleByName(String name);
}