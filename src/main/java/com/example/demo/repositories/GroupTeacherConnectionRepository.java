package com.example.demo.repositories;

import com.example.demo.models.GroupTeacherConnection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface GroupTeacherConnectionRepository extends JpaRepository<GroupTeacherConnection, Long> {

   List< GroupTeacherConnection> findByGroupId(Long id);

   List< GroupTeacherConnection >findByTeacherId(Long id);

    List<GroupTeacherConnection> findAllByTeacherId(Long id);

    GroupTeacherConnection findByGroupIdAndAndTeacherId(Long groupId, Long teacherId);
}