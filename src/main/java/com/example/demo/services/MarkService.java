package com.example.demo.services;

import com.example.demo.models.Mark;
import com.example.demo.models.Ticket;
import com.example.demo.repositories.MarkRepository;
import com.example.demo.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.AbstractQueue;
import java.util.List;

@Service
public class MarkService {
    @Autowired
    private final MarkRepository markRepository;

    @Autowired
    public MarkService(MarkRepository markRepository) {
        this.markRepository = markRepository;
    }

    public Mark getById(Long id) {
        return markRepository.getById(id);
    }

    public List<Mark> getAllByStudentId(Long id) {
        return markRepository.getAllByStudentId(id);
    }

    public List<Mark> getAllByTestId(Long id) {
        return markRepository.getAllByTestId(id);
    }
    public void addMark(Long testId,Long studentId, int mark){
        Mark m=new Mark(testId,studentId,mark);
        markRepository.save(m);
    }

    public Mark getByStudentIdAndTestId(Long studentId,Long testId){
        return markRepository.getByStudentIdAndTestId(studentId,testId);
    }
}
