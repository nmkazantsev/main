package com.example.demo.services;

import com.example.demo.models.*;
import com.example.demo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class GroupTeacherConnectionService {
    @Autowired
    private final GroupTeacherConnectionRepository groupTeacherConnectionRepository;

    @Autowired
    public GroupTeacherConnectionService(GroupTeacherConnectionRepository groupTeacherConnectionRepository, EntityManager entityManager) {
        this.groupTeacherConnectionRepository = groupTeacherConnectionRepository;
    }

    public List<GroupTeacherConnection> findByTeacherId(long id) {
        return groupTeacherConnectionRepository.findByTeacherId(id);
    }

    public List<GroupTeacherConnection> findByGroupId(long id) {
        return groupTeacherConnectionRepository.findByGroupId(id);
    }

    public List<GroupTeacherConnection> findAllByTeacherId(Long id) {
        return groupTeacherConnectionRepository.findAllByTeacherId(id);
    }

    public void addConnection(Long teacherId, Long classId) {
        //если такой свзяи нет
        if (groupTeacherConnectionRepository.findByGroupIdAndAndTeacherId(classId, teacherId) == null) {
            GroupTeacherConnection c = new GroupTeacherConnection(teacherId, classId);
            groupTeacherConnectionRepository.save(c);
            return;
        }
        System.out.println("in group teacher connection service ERORR: connection teacher "+
                String.valueOf(teacherId)+" to group "+String.valueOf(classId)+" already exists");

    }
}
