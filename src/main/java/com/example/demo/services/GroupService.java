package com.example.demo.services;

import com.example.demo.models.Group;

import com.example.demo.models.Person;
import com.example.demo.repositories.GroupRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;


@Service
public class GroupService {
    @Autowired
    private final GroupRepository groupRepository;

    @Autowired
    public GroupService(GroupRepository groupRepository, EntityManager entityManager) {
        this.groupRepository = groupRepository;
    }

    public Group findById(long id) {
        return groupRepository.getById(id);
    }

    public Group findByName(String name) {
        return groupRepository.findByName(name);
    }

    public void createGroup(String name) {
        Group g = new Group(name);
        groupRepository.save(g);
    }

    public boolean thisGroupExists(String login) {
        Group group = findByName(login);
        if (group != null) {
            return true;
        }
        return false;
    }
    public List<Group> getAll(){
        return groupRepository.getAll();
    }
}
