package com.example.demo.services;

import com.example.demo.models.Group;
import com.example.demo.models.Person;
import com.example.demo.models.Test;
import com.example.demo.models.Ticket;
import com.example.demo.repositories.GroupRepository;
import com.example.demo.repositories.PeopleRepository;
import com.example.demo.repositories.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TestService {
    @Autowired
    private final TestRepository testRepository;

    @Autowired
    TeacherService teacherService;

    @Autowired
    public TestService(TestRepository testRepository) {
        this.testRepository = testRepository;
    }

    public Test getById(Long id) {
        return testRepository.getById(id);
    }

    public Test getByName(String name) { return testRepository.getByName(name); }

    public List<Test> findAllByGroupId(Long id) {
        return testRepository.findAllByGroupId(id);
    }

    public void createTest(Date startTime, Date endTime, Long groupId, String name) {
        Test t = new Test(startTime, endTime, groupId, name);
        testRepository.save(t);
    }
    public boolean TestExists(String testName){
        return !(testRepository.findByName(testName)==null);
    }

    public List<Test> getAll(){
        return testRepository.getAll();
    }
}
