package com.example.demo.services;

import com.example.demo.models.Module;
import com.example.demo.repositories.ModuleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class ModuleService {
    @Autowired
    private final ModuleRepository moduleRepository;

    @Autowired
    TeacherService teacherService;

    @Autowired
    public ModuleService(ModuleRepository moduleRepository, EntityManager entityManager) {
        this.moduleRepository = moduleRepository;
    }

    public Module findModuleById(Long id) {
        return moduleRepository.findModuleById(id);
    }

    public List<Module> findAllByTeacherId(Long id) {
        return moduleRepository.findAllByTeacherId(id);
    }

    public Module findModuleByName(String name) {
        return moduleRepository.findModuleByName(name);
    }

    public void createModule(Long teacherId, String name) {
        Module m = new Module(teacherId, name);
        moduleRepository.save(m);
    }

    public void createModule(String name) {
        Module m = new Module(name);
        moduleRepository.save(m);
    }
    public void createModule(String name,Long teacherId) {
        Module m = new Module(teacherId,name);
        moduleRepository.save(m);
    }

    public boolean thisModuleExist(String name){
        if (findModuleByName(name)!=null){
            return true;
        } else {
            return false;
        }
    }

    public boolean moduleBelongsToTeacher(String moduleName,String teacherToken){
        List<Module> modules=findAllByTeacherId(teacherService.findByToken(teacherToken).getId());
        for(int i=0;i<modules.size();i++){
            if(modules.get(i).getName().equals(moduleName)){
                return true;
            }
        }
        return false;
    }
}
