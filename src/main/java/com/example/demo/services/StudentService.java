package com.example.demo.services;

import com.example.demo.models.Person;
import com.example.demo.models.Student;
import com.example.demo.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class StudentService {
    @Autowired
    private final StudentRepository studentRepository;

    @Autowired
   PersonService personService;

    @Autowired
    public StudentService(StudentRepository studentRepository, EntityManager entityManager) {
        this.studentRepository = studentRepository;
    }

    public Student findById(long id) {
        return studentRepository.getById(id);
    }

    public List< Student >findAllByGroupId(Long id){
        return studentRepository.findAllByGroupId(id);
    }

    public void addStudent(Long peopleId){
       Student s=new Student(null,peopleId);
       studentRepository.save(s);
    }

    public void addStudent(Long peopleId,Long groupId){
        Student s=new Student(groupId,peopleId);
        studentRepository.save(s);
    }

    public Student findByIdAndGroupId(Long id,Long groupId){
        return studentRepository.findByIdAndAndGroupId(id,groupId);
    }

    public Student findByPeopleId(Long id){
        return  studentRepository.findByPeopleId(id);
    }

    public Student findByName(String name){
        Person p= personService.findByLogin(name);
        return findByPeopleId(p.getId());
    }

    public void save(Student s){
        studentRepository.save(s);
    }

    public List<Student> getAll(){
        return studentRepository.getAll();
    }

}

