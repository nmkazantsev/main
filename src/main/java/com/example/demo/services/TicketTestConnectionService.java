package com.example.demo.services;

import com.example.demo.models.*;
import com.example.demo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class TicketTestConnectionService {
    @Autowired
    private final TicketTestConnectionRepository ticketTestConnectionRepository;

    @Autowired
    public TicketTestConnectionService(TicketTestConnectionRepository ticketTestConnectionRepository, EntityManager entityManager) {
        this.ticketTestConnectionRepository = ticketTestConnectionRepository;
    }

    public List<TicketTestConnection> findAllByTicketId(Long ticketId) {
        return ticketTestConnectionRepository.findAllByTicketId(ticketId);
    }

    public List<TicketTestConnection> findAllByTestId(Long testId) {
        return ticketTestConnectionRepository.findAllByTestId(testId);
    }

    public void createTicketTestConnection(long ticketId, long testId){
        ticketTestConnectionRepository.save(new TicketTestConnection(ticketId, testId));
    }

    public TicketTestConnection findByTicketIdAndTestId(Long ticketId, Long testId) {
        return ticketTestConnectionRepository.findByTicketIdAndTestId(ticketId, testId);
    }

    public void addConnection(Long ticketId, Long testId) {
        if (ticketTestConnectionRepository.findByTicketIdAndTestId(ticketId, testId) == null) {
            TicketTestConnection t = new TicketTestConnection(ticketId, testId);
            ticketTestConnectionRepository.save(t);
            return;
        }
        System.out.println("in ticket-test-connection service ERORR: connection ticket " +
                String.valueOf(ticketId) + " to Test " + String.valueOf(testId) + " already exists");

    }

    public boolean thisTicketTestConnectionExist(long ticketId, long testId){
       return !(ticketTestConnectionRepository.findByTicketIdAndTestId(ticketId,testId)==null);
    }

}
