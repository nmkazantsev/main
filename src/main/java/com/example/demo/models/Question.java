package com.example.demo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "question")
public class Question {
    @Column(name = "id")
    private Long id;
    @Column(name = "module_id")
    private Long moduleId;
    @Column(name = "text")
    private String text;
    @Column(name = "answer")
    private String answer;

    public Question() {
    }

    public Question(Long moduleId, String text, String answer) {
        setAnswer(answer);
        setText(text);
        setModuleId(moduleId);
    }

    public Question(String text, String answer) {
        setText(text);
        setAnswer(answer);
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GenericGenerator(name = "seq", strategy = "increment")
    @GeneratedValue(generator = "seq")
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public Long getModuleId() {
        return this.moduleId;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }
}



