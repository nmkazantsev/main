package com.example.demo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "ticket_test")
public class TicketTestConnection {
    @Column(name = "test_id")
    private Long testId;
    @Column(name = "ticket_id")
    private Long ticketId;
    @Column(name = "id")
    private Long id;

    public TicketTestConnection(Long ticketId, Long testId) {
       setTestId(testId);
        setTicketId(ticketId);
    }

    public TicketTestConnection() {
    }

    @Column(name = "ticket_id")
    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    @Column(name = "ticket_id")
    public Long getTicketId() {
        return this.ticketId;
    }

    @Column(name = "test_id")
    public void setTestId(Long testId) {
        this.testId = testId;
    }

    @Column(name = "test_id")
    public Long getTestId() {
        return this.testId;
    }


    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GenericGenerator(name = "seq", strategy = "increment")
    @GeneratedValue(generator = "seq")
    @Column(name = "id")
    public Long getId() {
        return id;
    }
}

