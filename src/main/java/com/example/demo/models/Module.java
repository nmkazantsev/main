package com.example.demo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

//это учебный модуль, а не часть приложения
@Entity
@Table(name = "module")
public class Module {
    @Column(name = "id")
    private Long id;
    @Column(name = "teacher_id")
    private Long teacherId;
    @Column(name = "name")
    private String name;

    public Module() {
    }

    public Module(Long teacherId, String name) {
        setTeacherId(teacherId);
        setName(name);
    }

    public Module( String name) {
        setName(name);
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GenericGenerator(name = "seq", strategy = "increment")
    @GeneratedValue(generator = "seq")
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getTeacherId() {
        return this.teacherId;
    }
}

